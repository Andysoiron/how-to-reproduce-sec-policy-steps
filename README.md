# This is a collection of steps to set up security policy features

It is meant to copy and past parts of it to the **How to set up and validate locally** section of MRs.

## Enable a feature flag

```bash
echo "Feature.enable(:feature_flag_name)" | rails c
```

## Enable pipeline execution experiment

1. Create a group
1. Go to **Settings** -> **General**
1. Toggle the **Permissions and group features** section
1. Enable **Security policy pipeline execution action**

## Group with scheduled scan execution

1. Create a group.
2. Enable instance wide [delayed project deletion](https://docs.gitlab.com/ee/user/admin_area/settings/visibility_and_access_controls.html#delayed-project-deletion)
3. Add projects to the group:
    1. Create a new blank project in the group
    2. Add a simple `.gitlab-ci.yml` file to the project. (See [simple `.gitlab-ci.yml` example](#example-files))
    3. Add a `package.json` file with content `{}` to the project. This is needed for the SAST scanner to run
4. Create a new scan execution policy with SAST.
    1. On the Group level left sidebar, select **Security & Compliance** and **Policies**
    2. Select **New Policy**
    3. Select **Scan execution policy**
    4. For **Name** choose any name
    5. For **Actions** choose **SAST**
    6. For **Conditions** choose: ![Screenshot_2023-05-23_at_12.53.19](https://gitlab.com/gitlab-org/gitlab/uploads/af60addebd681a9730576193eb29c0ea/Screenshot_2023-05-23_at_12.53.19.png)
    7. Alternatively to steps 1 to 6, you can switch to **.yaml mode** and copy the [scan execution policy example](#example-files) below.
    8. Select **Configure with a merge request**
    9. Merge the MR
5. If you haven't already, [set up a runner with docker](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md#docker-configuration)
6. To reset the timer and trigger the schedule worker, run:
```ruby
Security::OrchestrationPolicyRuleSchedule.update_all(next_run_at: Time.now - 1.day)
Security::OrchestrationPolicyRuleScheduleWorker.new.perform
```

## Project with MR that requires approval

1. Create a new project.
1. Add a `.gitlab-ci.yml` with secret detection (See [`.gitlab-ci.yml` with secret detection example](#example-files)).
1. Create a new security policy to require approval.
    1. On the projects left sidebar, select **Security & Compliance** and **Policies**.
    2. Select **New Policy**.
    3. Select **Scan result policy**.
    4. For **Name** choose any name.
    5. For **Rules** choose "when **Security Scan** **Secret Detection** runs against the **All protected branches** and find(s).
**Any** vulnerabilities that match all of the following criteria:".
    6. Select **Add new criteria**.
    7. Select **New Severity**.
    8. Select "Severity is: **All severity levels**".
    9. Select **Add new criteria**.
    10. Select **New Status**.
    11. Select Status is: **New** **All vulnerability stats**.
    12. In **Actions** Select "Require **1** approval from:".
    13. Select any user that is not you.
    7. Alternatively to steps 3 to 13, you can switch to **.yaml mode** and copy the [scan result policy example](#example-files) below. And replace the `user_approvers_ids` with a valid user ID that has access to the project.
    8. Select **Configure with a merge request**
    9. Merge the MR
1. If you haven't already, [set up a runner with docker](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/main/doc/howto/runner.md#docker-configuration)
1. Go to the project page and select the **Web IDE** button
1. Create a new file called `.env`
1. Add the following line to the file `AWS_TOKEN='AKIAZYONPI3G4JNCCWGA'`
1. Select the **source control** symbol on the left sidebar
1. Type any commit message
1. Select **Commit to new branch**
1. Press 'Enter' to confirm
1. Select **Create MR**
1. Select **Create merge request**
1. Wait for the pipeline to complete 

## Pipeline execution Policies

1. Create a group
1. Create a project in the group
1. Add a simple CI/CD config file to the project.
    ```
    # policy-ci.yml
    build1:
      stage: .pipeline-policy-pre
      script:
        - echo "Do your build here"
    ```
1. Go back to the group.
1. On the left sidebar, select **Security & Compliance** and **Policies**.
1. Select **New Policy** 
1. Select **Pipeline execution policy**
1. Choose a name for the policy
1. In the **Actions** section choose `Override` and select the project and `policy-ci.yml` file you created in step 2. and 3.
1. Select **Update via Merge Request**.
1. Merge the MR.
1. Go back to the group.
1. Create a new project
1. Go to **Build** and **Pipelines**.

### Example files

<details><summary>Simple `.gitlab-ci.yml`</summary>

```yaml
# .gitlab-ci.yml

image: busybox:latest

test1:
  stage: test
  script:
    - echo "Do a test here"
    - echo "For example run a test suite"

```
</details>

<details><summary>scan execution policy</summary>

```yaml
type: scan_execution_policy
name: test
description: ''
enabled: true
rules:
  - type: schedule
    branches:
      - '*'
    cadence: 0 0 * * *
actions:
  - scan: sast
    tags: []
```
</details>

<details><summary>`.gitlab-ci.yml` with secret detection</summary>

```yaml
# .gitlab-ci.yml

include:
  - template: Jobs/Secret-Detection.gitlab-ci.yml
test-job:
  script:
  - echo "Test Job..."

```
</details>

<details><summary>scan result policy</summary>

```yaml
type: scan_result_policy
name: Secrets
description: ''
enabled: true
rules:
  - type: scan_finding
    branches: []
    scanners:
      - secret_detection
    vulnerabilities_allowed: 0
    severity_levels:
      - critical
      - high
      - medium
      - low
      - unknown
      - info
    vulnerability_states:
      - new_needs_triage
      - new_dismissed
actions:
  - type: require_approval
    approvals_required: 1
    user_approvers_ids:
      - 4

```

</details>
